Session.set('template', {
  name: 'Example checklist',
  items: [
    'Sweep the floor',
    'Check the engines',
    'Replace the oil'
  ] 
});

Mesosphere({
  name: 'quickTaskForm',
  template: 'quickTask',
  method: function(result){
    $('.validation-error').remove();
    var validationResults = Mesosphere.quickTaskForm.validate(result);
    if(!validationResults.errors){
      // !! is a dirty hack to change to a boolean
      validationResults.formData.complete = !!validationResults.formData.complete;
      Tasks.insert(validationResults.formData);
    } else {
      var errors = validationResults.errors;
      for(var field in errors){
        $('[name=' + field + ']').after($('<div>').text(errors[field].message).addClass('validation-error').css('color', 'red'));
      }
    }
  },
  fields: {
    description: {
      required: true,
      transforms: ['clean'],
      requiredMessage: 'Please enter a description of your item'
    },
    name: {
      required: true,
      transforms: ['clean', 'capitalize'],
      requiredMessage: 'Please enter your name'
    }
  }
});

Mesosphere({
  name:"actionItemForm",
  template: 'actionItem',
  method: function(result){
    $('.validation-error').remove();
    var validationResults = Mesosphere.actionItemForm.validate(result);
    console.log(validationResults);
    if(!validationResults.errors){
      console.log(validationResults.formData);
      validationResults.formData.complete = true;
      Tasks.update(validationResults.formData.id, { $set: validationResults.formData });
      console.log("just before modal");
      $('#actionitemModal').modal('hide');
    } else {
      var errors = validationResults.errors;
      for(var field in errors){
        $('[name=actionItemForm]').find('[name=' + field + ']').after($('<div>').text(errors[field].message).addClass('validation-error').css('color', 'red'));
      }
    }
  },
  fields:{
    reason:{
      required:true,
      transforms:['clean', 'capitalize'],
      requiredMessage: 'Please enter a reason'
    },
    user: {
      required:true,
      transforms:['clean', 'capitalize'],
      requiredMessage: 'Please enter your name'
    }
  }
});

Mesosphere({
  name:"checklistForm",
  template: 'checklist',
  method: function(result){
    $('.validation-error').remove();
    var validationResults = Mesosphere.checklistForm.validate(result);
    console.log(validationResults);
    if(!validationResults.errors){
      console.log(validationResults.formData);
      validationResults.formData.complete = true;
      Tasks.update(validationResults.formData.id, { $set: validationResults.formData });
      $('#checklistModal').modal('hide');
    } else {
      var errors = validationResults.errors;
      for(var field in errors){
        $('[name=checklistForm]').find('[name=' + field + ']').after($('<div>').text(errors[field].message).addClass('validation-error').css('color', 'red'));
      }
    }
  },
  fields: checklistFields(Session.get('template'))
});

function checklistFields(checklist){
  var fields = checklist.items.reduce(function(fields, item, i){
    fields['status' + i] = {
      required: true,
      transforms: ['clean', 'capitalize']
    };

    return fields;
  }, {});

  fields.date = {
    required: true,
    requiredMessage: 'Please enter a date'
  };

  return fields;
}

Template.checklist.helpers({
  template: function(){
    return Session.get('template');
  }
});

Template.taskList.helpers({
  tasks: function(){
    return Tasks.find({$or: [{ complete:null }, {complete: false }]}, { sort: { description: 1 } });
  }
});

Template.taskLog.helpers({
  completedTasks: function(){
    return Tasks.find({complete: true}, { sort: { description: 1 } });
  }
});

Template.task.helpers({
  whenBadgeClass: function(event){
    var result = this.when == 'today' ? 'info' : 'danger';
    console.log(result);
    return result;
  }
});

Template.task.events({
  'click .toggle-task': function(event){
    if(this.complete){
      var complete = this.complete ? !this.complete : false;
      Tasks.update(this._id, { $set: { complete: complete } });
    } else {
      var form = $('#actionitemModal form');
      form.find('input[type=hidden]').remove();
      form.append('<input type="hidden" name="id" value="' + this._id + '" />');
      $('#actionitemModal').modal('show');
    }

    return false;
  },
  'click .delete-task': function(event){
    Tasks.remove(this._id);
  },
  'click .card': function(event){
    if(!this.complete){
      var form = $('#checklistModal form');
      form.find('input[type=hidden], .collection-item').remove();
      form.append('<input type="hidden" name="id" value="' + this._id + '" />');

      var template = Session.get('template');
      var itemsList = $('#checklistModal .checklist.collection')[0];
      template.items.forEach(function(item, i){
        Blaze.renderWithData(
          Template.radioGroup, 
          {
            name: item,
            index: i
          },
          itemsList
        );
      });

      $('#checklistModal').modal('show');
    }
  }
});
