Template.systemDetails.helpers({});

Template.readingCard.events({
    "click .graphs-link": function (event) {
        var _id = Template.parentData(2) == null ? Template.parentData(1)._id : Template.parentData(2)._id;
        var route = "/graphs/" + this.name + "/" + _id;
        Router.go(route);
        return false;
    }
});

Template.systemDetails.events({
    'click .back-link': function(){
        history.back();
    }
});

// Add Running hours fornm, Should add hours to all components also
Mesosphere({
    name: "runningHoursForm",
    template: "runningHours",
    method:function(result){
        $('.validation-error').remove();
        var validationResults = Mesosphere.runningHoursForm.validate(result);
        if(!validationResults.errors){
            var found = Systems.findOne({name: Session.get("systemName"), vesselId: 1, });
            if(found.runningHours){
                var newHours = parseInt(found.runningHours) + parseInt(validationResults.formData.hours);
                found.components.forEach(function(component){
                    component.runningHours = parseInt(component.runningHours) + parseInt(validationResults.formData.hours);
                });
                Systems.update(found._id, { $set: { runningHours: newHours, components: found.components } } );
            }
        } else{
            var errors = validationResults.errors;
            for(var field in errors) {
                $('[name=' + field + ']').after($('<div>').text(errors[field].message).addClass('validation-error').css('color', 'red'));
            }
        }
    },
    fields: {
        hours: {
            required: true,
            transforms: ['clean'],
            format: "integer",
            message: "Enter a positive integer between 1 and 24",
            rules:{
               maxValue:24,
               minValue:1
            },
            defaultValue: function(formFieldsObject){
                return 24;
            }
        } 
    }
});

// Usage 
Mesosphere({
    name: "usageForm",
    template: "usage",
    method:function(result){
        $('.validation-error').remove();
        var validationResults = Mesosphere.usageForm.validate(result);
        if(!validationResults.errors){
            var found = Systems.findOne({name:Session.get("systemName"), vesselId: 1, });
            console.log(found);
            if(found){
                Systems.update(found._id, { 
                    $set: { 
                        fuel: validationResults.formData.fuel, 
                        load: validationResults.formData.load,
                        rpm: validationResults.formData.rpm
                    }
                });
            }
        } else{
            var errors = validationResults.errors;
            for(var field in errors) {
                $('[name=' + field + ']').after($('<div>').text(errors[field].message).addClass('validation-error').css('color', 'red'));
            }
        }
    },
    fields: {
        fuel: {
            required: true,
            transforms: ['clean'],
            format: "integer",
            message: "Enter a positive integer",
            rules:{
               maxValue:1000,
               minValue:0
            },
            defaultValue: function(formFieldsObject){
                console.log(formFieldsObject);
                return formFieldsObject["fuel"];
            }
        },
        load: {
            required: true,
            transforms: ['clean'],
            format: "integer",
            message: "Enter a positive integer",
            rules:{
               maxValue:1000,
               minValue:0
            },
            defaultValue: function(formFieldsObject){
                return formFieldsObject["load"];
            }
        },
        rpm: {
            required: true,
            transforms: ['clean'],
            format: "integer",
            message: "Entera positive integer",
            rules:{
               maxValue:1000,
               minValue:0
            },
            defaultValue: function(formFieldsObject){
                return formFieldsObject["rpm"];
            }
        }
    }
});

Mesosphere({
    name: "PressureForm",
    template: "readingCard",
    method:function(result){
        $('.validation-error').remove();
        var validationResults = Mesosphere.PressureForm.validate(result);
        if(!validationResults.errors){
            var found = Systems.findOne({name: Session.get("systemName"), vesselId: 1, });
            if(found){
                for(var item in validationResults.formData){
                    found.readingGroups.forEach(function(group){
                        if(group.name == "Pressure"){
                            group.readingTypes.forEach(function(type){
                                if(type.name == item){
                                    type.readings.unshift({ time: new Date(),reading: parseInt(validationResults.formData[item]) })
                                }
                            });
                        }
                    });
                };
                Systems.update(found._id, found); 
            }
        } else{
            var errors = validationResults.errors;
            for(var field in errors) {
                $('[name=' + field + ']').after($('<div>').text(errors[field].message).addClass('validation-error').css('color', 'red'));
            }
        }
    },
    fields: { 
        Fuel: {
            required: false
        }
    }
});

Mesosphere({
    name: "TemperatureForm",
    template: "readingCard",
    method:function(result){
        $('.validation-error').remove();
        var validationResults = Mesosphere.TemperatureForm.validate(result);
        if(!validationResults.errors){
            var found = Systems.findOne({name: Session.get("systemName"), vesselId: 1, });
            if(found){
                for(var item in validationResults.formData){
                    found.readingGroups.forEach(function(group){
                        if(group.name == "Temperature"){
                            group.readingTypes.forEach(function(type){
                                if(type.name == item){
                                    type.readings.unshift({ time: new Date(),reading: parseInt(validationResults.formData[item]) })
                                }
                            });
                        }
                    });
                };
                Systems.update(found._id, found); 
            } else{
                var errors = validationResults.errors;
                for(var field in errors) {
                    $('[name=' + field + ']').after($('<div>').text(errors[field].message).addClass('validation-error').css('color', 'red'));
                }
            }
        }
    },
    fields: { 
        Fuel: {
            required: false
        }
    }
});