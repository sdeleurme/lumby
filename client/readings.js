Template.systems.helpers({
   systems: function(){
       return Systems.find({});
   }
});

Template.system.events({
  "click .card": function (e, o) {
    // clicking on the id takes you to the detail page
    Session.set("systemName", this.name);
    Router.go("/system-details/" + this._id);
  }
});