//var formStructure = generateFormStructure(3);

Template.formTest.rendered = function(){
	renderForm(formStructure);
}

function renderForm(formStructure){
	

	function renderLevel(levelStructure){

	}
}

function generateFormStructure(totalDepth){
	return generateLevel(0);

	function generateLevel(depth){
		var levelCount = getRandomInt(1, 6);
		var result = {};
		result.name = Fake.word();
		result.children = [];
		if(depth < totalDepth){
			for(var i = 0; i < levelCount; i++){
				result.children.push(generateLevel(depth+1));
			}
		}
		return result
	}
}

// Returns a random integer between min (included) and max (excluded)
// Using Math.round() will give you a non-uniform distribution!
// From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}