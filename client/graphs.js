if(Meteor.isClient){
    Template.graphs.events({
        'click .back-link': function(){
            history.back();
        }
    });
}

Template.graphs.rendered = function() {
    
    var axisLabel = this.data.readingGroup;
    var graphData = pressureTestData(this.data); 
    
    // Wrapping in nv.addGraph allows for '0 timeout render', stores rendered charts in nv.graphs,
    // and may do more in the future... it's NOT required
    nv.addGraph(function() {
        var chart = nv.models.lineChart()
            .useInteractiveGuideline(true)
            .x(function(d) { return d[0] })
            .y(function(d) { return d[1] })
            .color(d3.scale.category10().range())
            .duration(300)
            .clipVoronoi(false);
            
        chart.xAxis.tickFormat(function(d) {
            return d3.time.format('%H:00')(new Date(d))
        }).axisLabel('Time').ticks(d3.time.minutes, 15);
        
        chart.yAxis.tickFormat(d3.format('g')).axisLabel(axisLabel);
        
        d3.select('svg.engine-log-graph')
            .datum(graphData)
            .call(chart);
            
        nv.utils.windowResize(chart.update);
        
        return chart;
    });
    
    function pressureTestData(data) {
        var dataSet = [];
        var readingTypes;
        data.system.readingGroups.forEach(function(readingGroup) {
            if(readingGroup.name === data.readingGroup){
                readingTypes = readingGroup.readingTypes;    
            }
        })
        
        dataSet = readingTypes.map(function(element){
            var sortedPoints = element.readings.sort(function(a, b){ if(a.time < b.time) return -1; else {return 1}});
            var dataPoints = sortedPoints.map(function(reading){
                return [reading.time, reading.reading];
            });
            return {key: element.name, values: dataPoints};
        });
        console.log(dataSet);
        return dataSet;
        } 
}