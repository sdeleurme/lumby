// Global var because I saw it in the todos example
Tasks = new Meteor.Collection('tasks');
Systems = new Meteor.Collection('systems');


function timeMinusHours(hours){
    tempDate = new Date();
    tempDate.setHours(tempDate.getHours() - hours);
    return tempDate;
}

if (Meteor.isServer) {
    Meteor.startup(function () {
      if(Systems.find().count() === 0){
          console.log("inserting");
          var systems = [
               { 
                  name: "Port Main Engine", 
                  date: new Date(),
                  vesselId: 1,
                  runningHours: 1000,
                  load: 90,
                  rpm: 988,
                  fuel: 6,
                  components: [
                      {
                          name: "Air filter",
                          runningHours: 700
                      },
                      {
                          name: "Overhaul",
                          runningHours: 900
                      },
                      {
                          name: "Turbo",
                          runningHours: 150
                      },
                      {
                          name: "lube oil",
                          runningHours: 700
                      },
                      {
                          name: "fuel centrifuge",
                          runningHours: 900
                      },
                      {
                          name: "fuel filter",
                          runningHours: 150
                      },
                  ],
                  readingGroups: [
                      {
                          name: "Pressure",
                          readingTypes: [
                              {
                                  name: "Air Box",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 501
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 541
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 541
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 441
                                      },
                                  ]
                              },
                              {
                                  name: "Fuel",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 182
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 256
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 234
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 244
                                      },
                                  ]
                              },
                              {
                                  name: "lube out",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 350
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 320
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 340
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 310
                                      },
                                  ]
                              },
                              {
                                  name: "water in",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 405
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 399
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 435
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 489
                                      },
                                  ]
                              },
                              {
                                  name: "water out",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 404
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 350
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 434
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 492
                                      },
                                  ]
                              }
                          ]
                      },
                      {
                          name: "Temperature",
                          readingTypes: [
                              {
                                  name: "Air Box",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 220
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 180
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 190
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 230
                                      },
                                  ]
                              },
                              {
                                  name: "Lube",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 98
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 90
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 70
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 120
                                      },
                                  ]
                              },
                              {
                                  name: "lube out",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 120
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 110
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 90
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 150
                                      },
                                  ]
                              },
                              {
                                  name: "water in",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 50
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 70
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 60
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 65
                                      },
                                  ]
                              },
                              {
                                  name: "water out",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 70
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 90
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 80
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 86
                                      },
                                  ]
                              }
                          ]
                      }
                  ]
              },
              { 
                  name: "STBD Main Engine", 
                  date: new Date(),
                  vesselId: 1,
                  runningHours: 1000,
                  load: 90,
                  rpm: 988,
                  fuel: 6,
                  components: [
                      {
                          name: "Air filter",
                          runningHours: 700
                      },
                      {
                          name: "Overhaul",
                          runningHours: 900
                      },
                      {
                          name: "Turbo",
                          runningHours: 150
                      },
                      {
                          name: "lube oil",
                          runningHours: 700
                      },
                      {
                          name: "fuel centrifuge",
                          runningHours: 900
                      },
                      {
                          name: "fuel filter",
                          runningHours: 150
                      },
                  ],
                  readingGroups: [
                      {
                          name: "Pressure",
                          readingTypes: [
                              {
                                  name: "Air Box",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 501
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 541
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 541
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 441
                                      },
                                  ]
                              },
                              {
                                  name: "Fuel",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 182
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 256
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 234
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 244
                                      },
                                  ]
                              },
                              {
                                  name: "lube out",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 350
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 320
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 340
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 310
                                      },
                                  ]
                              },
                              {
                                  name: "water in",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 405
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 399
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 435
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 489
                                      },
                                  ]
                              },
                              {
                                  name: "water out",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 404
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 350
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 434
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 492
                                      },
                                  ]
                              }
                          ]
                      },
                      {
                          name: "Temperature",
                          readingTypes: [
                              {
                                  name: "Air Box",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 220
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 180
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 190
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 230
                                      },
                                  ]
                              },
                              {
                                  name: "Lube",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 98
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 90
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 70
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 120
                                      },
                                  ]
                              },
                              {
                                  name: "lube out",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 120
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 110
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 90
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 150
                                      },
                                  ]
                              },
                              {
                                  name: "water in",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 50
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 70
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 60
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 65
                                      },
                                  ]
                              },
                              {
                                  name: "water out",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 70
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 90
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 80
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 86
                                      },
                                  ]
                              }
                          ]
                      }
                  ]
              },
              { 
                  name: "Center Main Engine", 
                  date: new Date(),
                  vesselId: 1,
                  runningHours: 1000,
                  load: 90,
                  rpm: 988,
                  fuel: 6,
                  components: [
                      {
                          name: "Air filter",
                          runningHours: 700
                      },
                      {
                          name: "Overhaul",
                          runningHours: 900
                      },
                      {
                          name: "Turbo",
                          runningHours: 150
                      },
                      {
                          name: "lube oil",
                          runningHours: 700
                      },
                      {
                          name: "fuel centrifuge",
                          runningHours: 900
                      },
                      {
                          name: "fuel filter",
                          runningHours: 150
                      },
                  ],
                  readingGroups: [
                      {
                          name: "Pressure",
                          readingTypes: [
                              {
                                  name: "Air Box",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 501
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 541
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 541
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 441
                                      },
                                  ]
                              },
                              {
                                  name: "Fuel",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 182
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 256
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 234
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 244
                                      },
                                  ]
                              },
                              {
                                  name: "lube out",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 350
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 320
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 340
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 310
                                      },
                                  ]
                              },
                              {
                                  name: "water in",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 405
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 399
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 435
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 489
                                      },
                                  ]
                              },
                              {
                                  name: "water out",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 404
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 350
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 434
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 492
                                      },
                                  ]
                              }
                          ]
                      },
                      {
                          name: "Temperature",
                          readingTypes: [
                              {
                                  name: "Air Box",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 220
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 180
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 190
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 230
                                      },
                                  ]
                              },
                              {
                                  name: "Lube",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 98
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 90
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 70
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 120
                                      },
                                  ]
                              },
                              {
                                  name: "lube out",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 120
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 110
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 90
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 150
                                      },
                                  ]
                              },
                              {
                                  name: "water in",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 50
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 70
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 60
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 65
                                      },
                                  ]
                              },
                              {
                                  name: "water out",
                                  readings: [
                                      {
                                          time: timeMinusHours(4),
                                          reading: 70
                                      },
                                      {
                                          time: timeMinusHours(3),
                                          reading: 90
                                      },
                                      {
                                          time: timeMinusHours(2),
                                          reading: 80
                                      },
                                      {
                                          time: timeMinusHours(1),
                                          reading: 86
                                      },
                                  ]
                              }
                          ]
                      }
                  ]
              }
          ];
          systems.forEach(function(system){
              Systems.insert(system);    
          });
      }
    });
}