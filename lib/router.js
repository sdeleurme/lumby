//                      __           
//    _________  __  __/ /____  _____
//   / ___/ __ \/ / / / __/ _ \/ ___/
//  / /  / /_/ / /_/ / /_/  __/ /    
// /_/   \____/\__,_/\__/\___/_/     
//                                   
if (Meteor.isClient) {
  // Set this as true to avoid nav issues on reload 
  Iron.Location.configure({useHashPaths: false}); 
}

Router.route('/tasks', function(){
    this.render('tasks');
});

Router.route('/readings', function(){
    this.render('readings');
});

Router.route('/system-details/:_id', function () {
    this.render('systemDetails',{
      data: function(){
        return Systems.findOne({_id: this.params._id });
      }
    });
});

Router.route('/graphs/:_readingGroup/:_id', function () {
    console.log('Make some graphs.');
    this.render('graphs',{
      data: function(){
        return { 
          system: Systems.findOne({_id: this.params._id }),
          readingGroup: this.params._readingGroup
        }
      }
    });
});

Router.route('/', function () {
  if (this.ready()){
    this.render('tasks');
  } else {
    this.render('loading'); // TODO: doesn't exist yet
  }
});

Router.configure({
  layoutTemplate: 'layout',
  notFoundTemplate: 'notFound',
  loadingTemplate: 'loading'
});

Router.onAfterAction(function(){
  $(".main, .sidenav").removeClass("menu-open");
  $("body").removeClass("hide-scroll");
});